from app.CONSTANTS import *
from app.error import *
from app.weather_reporter import WeatherReporter


def get_temperature_report(start_datestring: str, end_datestring: str, city):
    start_date = WeatherReporter.extract_date_from_datestring(start_datestring)
    end_date = WeatherReporter.extract_date_from_datestring(end_datestring)
    temperatures = WeatherReporter.get_temperatures_of_date_range(start_date, end_date, city)
    report_lines = [REPORT_HEADING(start_datestring, end_datestring, city)]
    report_lines.append(len(report_lines[0])* "-" + '\n')
    report_lines.append("DATE\t\tTEMPERATURE\t\tMESSAGE")
    for date, temp in temperatures.items():
        date_str = '/'.join(map(str, date[::-1]))
        report_lines.append(date_str + '\t\t' + str(temp) + '\t\t' + WeatherReporter.get_message_corresponding_to_temperature(temp))
    return "\n".join(report_lines)

if __name__ == "__main__":
    start_date = input("Enter Starting Date: ")
    end_date = input("Enter Ending Date: ")
    city = input("Enter City: ")

    try:
        report = get_temperature_report(start_date, end_date, city)
        print(report)
    except Exception as e:
        print(str(e))
