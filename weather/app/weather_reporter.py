from app.error import *
from app.CONSTANTS import *
import datetime
import random
from typing import Tuple


class WeatherReporter:
    @staticmethod
    def extract_date_from_datestring(datestring: str):
        datestring_splitted = datestring.split('/')
        try:
            day, month, year = map(int, datestring_splitted)
        except:
            raise InvalidDateFormatException
        try:
            datetime.date(year, month, day)
        except:
            raise InvalidDateException
        return (year, month, day)

    @staticmethod
    def get_next_date(current_date: Tuple[int, int, int]):
        current_date_obj = datetime.date(*current_date)
        next_date_obj = current_date_obj + datetime.timedelta(days=1)
        return next_date_obj.timetuple()[0:3]

    @staticmethod
    def get_difference_in_days_between_two_dates(earlier_date: Tuple[int, int, int], later_date: Tuple[int, int, int]):
        timedelta = datetime.date(*later_date) - datetime.date(*earlier_date)
        return timedelta.days

    @staticmethod
    def get_temperature_of_date(date: Tuple[int, int, int], city: str):
        if city.strip().lower() not in CITIES:
            raise CityNotFoundException
        seed = (datetime.date(*date) - datetime.date(1, 1, 1)).days + sum([ord(c) for c in city.strip().lower()])
        random.seed(seed)
        return random.randint(-100, 100)

    @staticmethod
    def get_temperatures_of_date_range(start_date: Tuple[int, int, int], end_date: Tuple[int, int, int], city: str):
        if (datetime.date(*end_date) - datetime.date(*start_date)).days < 0:
            raise InvalidDateRangeException 
        total_consecutive_days = 1 + WeatherReporter.get_difference_in_days_between_two_dates(start_date, end_date)
        temperature_values = {}
        current_date = start_date
        for _ in range(total_consecutive_days):
            temperature_values[current_date] = WeatherReporter.get_temperature_of_date(current_date, city)
            current_date = WeatherReporter.get_next_date(current_date)
        return temperature_values
    
    @staticmethod
    def get_message_corresponding_to_temperature(temperature):
        if temperature > 54:
            return MESSAGE_ABOVE_54
        if temperature >= 41:
            return MESSAGE_41_TO_54
        if temperature >= 32:
            return MESSAGE_32_TO_40
        if temperature >= -30:
            return MESSAGE_MINUS_30_TO_PLUS_31
        if temperature < -30:
            return MESSAGE_BELOW_MINUS_30
