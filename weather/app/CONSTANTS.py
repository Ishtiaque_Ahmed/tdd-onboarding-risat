CITIES = ['dhaka', 'chittagong', 'kolkata', 'delhi', 'kathmandu', 'tokyo', 'beijing']

InvalidDateFormatException_MESSAGE = "The date should be a string in DD/MM/YYYY format"
InvalidDateException_MESSAGE = "Date is invalid"
CityNotFoundException_MESSAGE = "City not available"
InvalidDateRangeException_MESSAGE = "The end date is earlier than the starting date"

REPORT_HEADING = lambda date_from, date_to, city : "\n\nWeather report of " + city + " from " + date_from + " to " + date_to

MESSAGE_BELOW_MINUS_30 = "EXTREME COLD WARNING"
MESSAGE_32_TO_40 = "heat cramps"
MESSAGE_41_TO_54 = "Heat exhaustion"
MESSAGE_ABOVE_54 = "Extreme heat: heatstroke"
MESSAGE_MINUS_30_TO_PLUS_31 = "OK"

