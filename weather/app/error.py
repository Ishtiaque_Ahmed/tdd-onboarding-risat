from app.CONSTANTS import *

class InvalidDateFormatException(Exception):
    def __str__(self):
        return InvalidDateFormatException_MESSAGE

class InvalidDateException(Exception):
    def __str__(self):
        return InvalidDateException

class CityNotFoundException(Exception):
    def __str__(self):
        return CityNotFoundException_MESSAGE

class InvalidDateRangeException(Exception):
    def __str__(self):
        return InvalidDateRangeException_MESSAGE
