from app.CONSTANTS import *
from app.error import *
from app.weather_reporter import WeatherReporter
import unittest


class WeatherReporterTester(unittest.TestCase):
    def test_weather_reporter_extract_date_from_datestring_returns_correct_result(self):
        self.assertEqual(WeatherReporter.extract_date_from_datestring("23/2/2018"), (2018, 2, 24))

    def test_weather_reporter_extract_date_from_datestring_returns_error_for_invalid_format(self):
        self.assertRaises(InvalidDateFormatException, WeatherReporter.extract_date_from_datestring, "23-2-2018")

    def test_weather_reporter_extract_date_from_datestring_returns_error_for_invalid_date(self):
        self.assertRaises(InvalidDateException, WeatherReporter.extract_date_from_datestring, "31/2/2018")

    def test_weather_reporter_get_next_date_returns_correct_result(self):
        self.assertEqual(WeatherReporter.get_next_date((2020, 2, 28)), (2020, 2, 29))

    def test_weather_reporter_get_difference_in_days_between_two_dates_returns_correct_result(self):
        self.assertEqual(WeatherReporter.get_difference_in_days_between_two_dates((2000, 2, 25), (2000, 3, 2)), 6)

    def test_weather_reporter_get_temperature_of_date_returns_correct_result(self):
        self.assertEqual(WeatherReporter.get_temperature_of_date((2021, 5, 5), 'Dhaka'), -35)
    def test_weather_reporter_get_temperature_of_date_returns_error_for_unavailable_city(self):
        self.assertRaises(CityNotFoundException, WeatherReporter.get_temperature_of_date, (2021, 2, 3), 'London')

    def test_weather_reporter_get_temperatures_of_date_range_returns_correct_result(self):
        self.assertEqual(WeatherReporter.get_temperatures_of_date_range((2021, 5, 5), (2021, 5, 11), 'Dhaka'), 
        {(2021, 5, 5): -35,
        (2021, 5, 6): -53,
        (2021, 5, 7): -69,
        (2021, 5, 8): 17,
        (2021, 5, 9): -91,
        (2021, 5, 10): 79,
        (2021, 5, 11): 81})
    def test_weather_reporter_get_temperatures_of_date_range_returns_error_for_wrong_range(self):
        self.assertRaises(InvalidDateRangeException, WeatherReporter.get_temperatures_of_date_range, (2020, 10, 1), (2020, 9, 23), 'Dhaka')

    def test_weather_reporter_get_message_corresponding_to_temperature(self):
        self.assertEqual(WeatherReporter.get_message_corresponding_to_temperature(38), MESSAGE_32_TO_40)
