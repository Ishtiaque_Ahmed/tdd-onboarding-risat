from app.CONSTANTS import *


class InvalidDateFormatException(Exception):
    def __str__(self):
        return InvalidDateFormatException_MESSAGE

class KidsNotAccompaniedByAdultException(Exception):
    def __str__(self):
        return KidsNotAccompaniedByAdultException_MESSAGE
