from app.error import *
import numpy as np
from typing import List
import datetime
from app.CONSTANTS import *


class TicketBooker:
    

    @staticmethod
    def is_weekend(day: int, month: int, year: int):
        weekday_id = datetime.date(year, month, day).weekday()
        weekday = ID2WEEKDAY[weekday_id]
        return weekday in WEEKEND_DAYS

    @staticmethod
    def extract_day_month_year_from_datestring(datestring: str):
        datestring_splitted = datestring.split('/')
        try:
            day, month, year = map(int, datestring_splitted)
        except:
            raise InvalidDateFormatException
        return day, month, year

    @staticmethod
    def is_kid(age: int):
        return age < KIDS_AGE_LIMIT

    @staticmethod
    def is_group_eligible_for_travel(age_list: List[int]):
        is_kid_list = [TicketBooker.is_kid(age) for age in age_list]
        is_all_kid = np.all(is_kid_list)
        return not is_all_kid

    def __init__(self, baseprice: float, discount_percentage: float):
        self.set_baseprice(baseprice)
        self.set_weekend_discount_percentage(discount_percentage)

    def set_baseprice(self, price: float):
        self.baseprice = price

    def set_weekend_discount_percentage(self, discount: float):
        self.weekend_discount_percentage = discount

    def calculate_fare(self, age_list: List[int], is_weekend: bool):
        mask = 1 - np.array([TicketBooker.is_kid(age) for age in age_list]).astype('int')*0.5
        gross_fare = (mask*self.baseprice).sum()
        return gross_fare*(100-self.weekend_discount_percentage)/100 if is_weekend else gross_fare        

    def book_ticket(self, departure_datestring: str, city: str, headcount: int, age_list: List[int], arrival_datestring: str = None):
        departure_date = TicketBooker.extract_day_month_year_from_datestring(departure_datestring)
        oneway = True
        if arrival_datestring:
            oneway = False
            arrival_date = TicketBooker.extract_day_month_year_from_datestring(arrival_datestring)

        if not TicketBooker.is_group_eligible_for_travel(age_list):
            raise KidsNotAccompaniedByAdultException
    
        is_departure_on_weekend = TicketBooker.is_weekend(*departure_date)
        departure_fare = self.calculate_fare(age_list, is_departure_on_weekend)

        arrival_fare = 0
        if not oneway:
            is_arrival_on_weekend = TicketBooker.is_weekend(*arrival_date)
            arrival_fare = self.calculate_fare(age_list, is_arrival_on_weekend)
        
        return departure_fare + arrival_fare
