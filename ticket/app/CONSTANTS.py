ID2WEEKDAY = {0: 'monday', 1: 'tuesday', 2: 'wednesday', 3: 'thursday', 4: 'friday', 5: 'saturday', 6: 'sunday'}
WEEKDAY2ID = {'monday': 0, 'tuesday': 1, 'wednesday': 2, 'thursday': 3, 'friday': 4, 'saturday': 5, 'sunday': 6}
WEEKEND_DAYS = ['friday', 'saturday']

KIDS_AGE_LIMIT = 15

InvalidDateFormatException_MESSAGE = "The date should be a string in DD/MM/YYYY format"
KidsNotAccompaniedByAdultException_MESSAGE = "Kids cannot travel alone without being accompanied by an adult"
