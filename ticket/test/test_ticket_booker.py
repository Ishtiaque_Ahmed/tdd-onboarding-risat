import unittest
from app.ticket_booker import TicketBooker
from app.error import *
 
 
class TicketBookerTester(unittest.TestCase):

    def test_ticketbooker_extract_day_month_year_from_datestring_returns_correct_result(self):
        self.assertEqual(TicketBooker.extract_day_month_year_from_datestring("23/2/2018"), (23, 2, 2018))

    def test_ticketbooker_extract_day_month_year_from_datestring_returns_error_for_invalid_format(self):
        self.assertRaises(InvalidDateFormatException, TicketBooker.extract_day_month_year_from_datestring, "23-2-2018")

    def test_ticketbooker_book_ticket_returns_error_for_no_adults(self):
        self.assertRaises(KidsNotAccompaniedByAdultException, TicketBooker(100, 5).book_ticket, "23/2/2018", "Dhaka", 4, [12, 5, 14, 13])

    def test_ticketbooker_is_weekend_returns_correct_result(self):
        self.assertEqual(TicketBooker.is_weekend(29, 5, 2021), True)

    def test_ticketbooker_calculate_fare_returns_correct_result_on_weekend(self):
        self.assertEqual(TicketBooker(100, 40).calculate_fare([20, 18, 35, 8], True), 210)

    def test_ticketbooker_calculate_fare_returns_correct_result_on_weekday(self):
        self.assertEqual(TicketBooker(100, 40).calculate_fare([20, 18, 35, 8], False), 350)

    def test_ticketbooker_bookticket_returns_correct_result_for_oneway_ticket(self):
        self.assertEqual(TicketBooker(100, 40).book_ticket("29/5/2021", "Dhaka", 5, [20, 18, 35, 8, 14]), 240)
        
    def test_ticketbooker_bookticket_returns_correct_result_for_return_ticket(self):
        self.assertEqual(TicketBooker(100, 40).book_ticket("29/5/2021", "Dhaka", 5, [20, 18, 35, 8, 14], "2/6/2021"), 640)
     
#if __name__ == '__main__':
#    unittest.main()

