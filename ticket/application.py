from typing import Optional, List
from fastapi import FastAPI, Query
from app.ticket_booker import TicketBooker
from pydantic import BaseModel
from app.error import *


app = FastAPI()
__booker = TicketBooker(1000, 10)

class BookResponse(BaseModel):
    message: str
    total_price: float

class SetFareResponse(BaseModel):
    message: str

@app.get("/set-fare")
def set_fare(baseprice: float, discount: float):
    global __booker
    __booker.set_baseprice(baseprice)
    __booker.set_weekend_discount_percentage(discount)
    return SetFareResponse(message="success")

@app.get("/book-ticket")
def book_ticket(departure_date: str, city: str, headcount: int = Query(..., gt=0), age: List[int] = Query(..., ge=0), arrival_date: Optional[str] = None):
    price = 0
    try:
        price = __booker.book_ticket(departure_date, city, headcount, age, arrival_date)
        message = "ticket booked"
    except InvalidDateFormatException as IDFerror:
        message = str(IDFerror)
    except KidsNotAccompaniedByAdultException as KNAAerror:
        message = str(KNAAerror)

    return BookResponse(message=message, total_price=price)
    

"""    http://127.0.0.1:8000/book-ticket?departure_date=27/5/2021&headcount=4&age=25&age=10&age=34&age=32&city=dhaka&arrival_date=29/5/2021    """
"""    http://127.0.0.1:8000/set-fare?baseprice=100&discount=10 """

