import unittest
from app.main import *
 
 
class MainTester(unittest.TestCase):
    def test_get_space_separated_numbers_from_list(self):
        expected_outputs = [
                            ([1], "1"),
                            ([1,2], "1 2"),
                            ([1,2,3], "1 2 3"),
                            ([], "")
                        ]
        for _input, _output in expected_outputs:
            self.assertEqual(_output, get_space_separated_numbers_from_list(_input))

    def test_get_final_result(self):
        expected_outputs = {
                        1: "0 1 1",
                        2:"0 1 1 2",
                        3: "0 1 1 2 3",
                        4: "0 1 1 2 3",
                        5: "0 1 1 2 3 5",
                        8: "0 1 1 2 3 5 8",
                        12: "0 1 1 2 3 5 8",
                        13: "0 1 1 2 3 5 8 13",
                        54: "0 1 1 2 3 5 8 13 21 34",
                        55: "0 1 1 2 3 5 8 13 21 34 55",
                        80: "0 1 1 2 3 5 8 13 21 34 55"
                    }
        for _input, _output in expected_outputs.items():
            self.assertEqual(_output, get_final_result(_input))

#if __name__ == '__main__':
#    unittest.main()