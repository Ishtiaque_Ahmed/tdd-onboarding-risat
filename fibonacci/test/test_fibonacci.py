import unittest
from app.fibonacci import FibonacciGenerator
from app.error import *
 
 
class FibonacciTester(unittest.TestCase):

    def test_fibonaccigenerator_get_next_fib_method_returns_correct_result(self):
        expected_sequence = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
        fibonacci_generator = FibonacciGenerator()
        for val in expected_sequence:
            self.assertEqual(fibonacci_generator.get_next_fib(), val)

    def test_fibonaccigenerator_get_fibonacci_sequence_till_n_method_returns_correct_result(self):
        test_cases = {
                        1: [0, 1, 1],
                        2:[0, 1, 1, 2],
                        3: [0, 1, 1, 2, 3],
                        4: [0, 1, 1, 2, 3],
                        5: [0, 1, 1, 2, 3, 5],
                        6: [0, 1, 1, 2, 3, 5],
                        7: [0, 1, 1, 2, 3, 5],
                        8: [0, 1, 1, 2, 3 ,5 ,8],
                        12: [0, 1, 1, 2, 3, 5, 8],
                        13: [0, 1, 1, 2, 3, 5, 8, 13],
                        54: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34],
                        55: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55],
                        80: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
                    }
        for _input, _output in test_cases.items():
            fibonacci_generator = FibonacciGenerator()
            self.assertEqual(fibonacci_generator.get_fibonacci_sequence_till_n(_input), _output)

    def test_fibonaccigenerator_returns_error_message_for_nonpositive_input(self):
        self.assertRaises(NonPositiveValueException, FibonacciGenerator().get_fibonacci_sequence_till_n, 0)
        self.assertRaises(NonPositiveValueException, FibonacciGenerator().get_fibonacci_sequence_till_n, -1)

    def test_fibonaccigenerator_returns_error_message_for_nonnumber_input(self):
        self.assertRaises(NonNumberException, FibonacciGenerator().get_fibonacci_sequence_till_n, "")
        self.assertRaises(NonNumberException, FibonacciGenerator().get_fibonacci_sequence_till_n, [])
         
#if __name__ == '__main__':
#    unittest.main()