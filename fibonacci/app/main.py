from app.fibonacci import FibonacciGenerator


def get_space_separated_numbers_from_list(l):
    return " ". join(map(str, l))

def get_final_result(x):
    fibonacci_sequence = FibonacciGenerator().get_fibonacci_sequence_till_n(x)
    result_string = get_space_separated_numbers_from_list(fibonacci_sequence)
    return result_string
