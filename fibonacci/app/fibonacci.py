from app.error import *

class FibonacciGenerator:
    def __init__(self):
        self.reset()

    def reset(self):
        self.current = 0
        self.next = 1

    def get_next_fib(self):
        result_to_return = self.current
        next_next = self.current + self.next
        self.current = self.next
        self.next = next_next
        return result_to_return

    def get_fibonacci_sequence_till_n(self, n):
        number_types = (int, float)
        if not isinstance(n, number_types):
            raise NonNumberException

        if n <= 0:
            raise NonPositiveValueException
        result = []
        x = self.get_next_fib()
        while x <= n:
            result.append(x)
            x = self.get_next_fib()
        return result
