class NonPositiveValueException(Exception):
    def __str__(self):
        return "Only positive numbers as range"

class NonNumberException(Exception):
    def __str__(self):
        return "Only numbers as function parameter"
